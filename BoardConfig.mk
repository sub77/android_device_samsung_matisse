#
# Copyright (C) 2011 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

TARGET_CPU_ABI	 				:= armeabi-v7a
TARGET_CPU_ABI2 				:= armeabi
TARGET_CPU_SMP 					:= true
TARGET_ARCH 					:= arm
TARGET_ARCH_VARIANT 			:= armv7-a-neon
TARGET_CPU_VARIANT 				:= krait

TARGET_NO_BOOTLOADER 			:= true

TARGET_NO_RADIOIMAGE 			:= true
TARGET_BOARD_PLATFORM 			:= msm8226
TARGET_BOOTLOADER_BOARD_NAME 	:= MSM8226

#BOARD_MKBOOTIMG_ARGS			:= --kernel_offset 0x0000000 --ramdisk_offset 0x02000000 --second_offset 0x00f00000 --tags_offset 0x01e00000 --dt device/samsung/matisse/dtb
#BOARD_MKBOOTIMG_ARGS 			:= --ramdisk_offset 0x02000000 --tags_offset 0x1e00000 --dt device/samsung/matisse/dtb

BOARD_CUSTOM_BOOTIMG_MK			:= device/samsung/matissewifi/mkbootimg.mk
BOARD_KERNEL_BASE 				:= 0x00000000
BOARD_KERNEL_CMDLINE 			:= console=null androidboot.console=null androidboot.hardware=qcom user_debug=31 msm_rtb.filter=0x37 ehci-hcd.park=3
BOARD_KERNEL_PAGESIZE 			:= 2048
BOARD_KERNEL_SEPARATED_DT 		:= true
BOARD_MKBOOTIMG_ARGS 			:= --ramdisk_offset 0x02000000 --tags_offset 0x1e00000
TARGET_KERNEL_SOURCE 			:= kernel/samsung/matissewifi
TARGET_KERNEL_CONFIG 			:= msm8226-sec_defconfig
TARGET_KERNEL_VARIANT_CONFIG 	:= msm8226-sec_matissewifi_defconfig

#TWRP flags
DEVICE_RESOLUTION 				:= 1280x800
TW_NO_USB_STORAGE 				:= true
TW_INCLUDE_JB_CRYPTO 			:= true
RECOVERY_SDCARD_ON_DATA 		:= true
BOARD_SUPPRESS_SECURE_ERASE 	:= true

# Recovery
TARGET_RECOVERY_PIXEL_FORMAT := "BGRA_8888"
BOARD_USE_CUSTOM_RECOVERY_FONT := \"roboto_23x41.h\"

TARGET_USERIMAGES_USE_EXT4 := true
BOARD_BOOTIMAGE_PARTITION_SIZE := 14485760
BOARD_RECOVERYIMAGE_PARTITION_SIZE := 16485760
BOARD_SYSTEMIMAGE_PARTITION_SIZE := 1073741824

BOARD_USERDATAIMAGE_PARTITION_SIZE := 2147483648
BOARD_FLASH_BLOCK_SIZE := 131072 # (BOARD_KERNEL_PAGESIZE * 64)
BOARD_VOLD_EMMC_SHARES_DEV_MAJOR := true


